from .html import html_minify
from .css import css_minify
from .js import js_minify
from .sass import sass_minify
from .png import png_minify
from .jpeg import jpeg_minify
from .svg import svg_minify

__all__ = [html_minify, css_minify, js_minify, sass_minify, png_minify, jpeg_minify, svg_minify]
